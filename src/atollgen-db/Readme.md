# AtollGen Database

Python library used to build the AtollGen DB, starting point of the AtollGen
pipeline. The pipeline is available at
https://gitlab.com/atollgen/atollgen-pipeline. The documentation of the project
is available at https://atollgen.gitlab.io/docs.
