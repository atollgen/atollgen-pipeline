#!/usr/bin/env python
# coding=utf-8


import re
from dataclasses import fields
from typing import Optional

import numpy as np
import pandas as pd
from loguru import logger
from pydantic import Field
from pydantic.dataclasses import dataclass

logger.remove()

accession_re = re.compile(r"(([A-Z]+|[A-Z]+_[A-Z]*)[0-9]+(_\w+)?)")


def other_to_accession(other: str) -> Optional[str]:
    """other_to_accession

    Args:
        other (str): The other string that could contain one or more accession number

    Returns:
        Optional[Tuple[str, str]]: the first match of an accession number if any, None otherwise.
    """
    matches = accession_re.findall(other)
    if not matches:
        return None
    matches = [match[0] for match in matches]
    accession = matches[0]
    return accession


@dataclass
class Island:
    """Data container that represent an Island."""

    accession: str
    start: int
    end: int
    seq_len: int = Field(init=False, default=0)
    version: Optional[int] = None
    insertion: str = ""
    detection: str = ""
    organism: str = ""
    reference: str = ""
    type_: str = ""
    other: str = ""

    def __post_init_post_parse__(self):
        self._convert_accession()
        self.start, self.end = sorted([self.start, self.end])
        self.seq_len = np.abs(self.end - self.start)

    def _convert_accession(self):
        caption = self.accession.split(".")[0]
        if not self.version and len(self.accession.split(".")) > 1:
            try:
                self.version = self.accession.split(".")[-1]
            except ValueError:
                pass
        self.accession = caption.strip()

    @classmethod
    @property
    def columns(cls):
        return [field.name for field in fields(cls)]

    @staticmethod
    def to_df(islands):
        df = pd.DataFrame(islands)
        return df

    @classmethod
    def from_df(cls, df):
        return [
            cls(**row) for row in df.reindex(columns=cls.columns).to_dict("records")
        ]
