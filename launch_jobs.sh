#!/bin/sh

#SBATCH --array               1-131
#SBATCH --account             ...
#SBATCH --cpus-per-task       4
#SBATCH --error               /.../logs/slurm-%j.err
#SBATCH --mem-per-cpu         2000M
#SBATCH --output              /.../logs/slurm-%j.out
#SBATCH --time                0:32:00

export TEMPDIR=$SLURM_TMPDIR
atollgen-cli batch --db-dir /...atollgen-pipeline/data/interim /...atollgen-pipeline/data/interim/islands_failed.csv /.../atollgen-outputs $SLURM_ARRAY_TASK_ID/131 --n-jobs=2
