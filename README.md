# Atollgen pipeline

Atollgen pipeline for research reproducibility, part of the [Atollgen project].

This pipeline is mainly based on the atollgen-cli tool, which is a command line
tool that allow genomic island classification and caracterization.

This repository contains the pipeline that allow to reproduce the results of an
ongoing paper, from the frozen input data (stored in a [Zenodo record]) to the
final results (tables and figures) available on the paper.

## Installation

The recommended way to install the pipeline is to clone the repository, then use
conda to install the dependencies:

```bash
git clone
cd atollgen-pipeline
conda env create -f environment.yml
conda activate atollgen-pipeline
```

All the dependencies will be installed in a conda environment named
`atollgen-pipeline`. They are frozen to a specific version, so that the
pipeline can be reproduced in the future.

For the post-process step, you will also need the Julia programming language
(v1.9). A Project.toml file is provided, so that you can install the
dependencies with the following command:

```bash
julia --project=. -e 'using Pkg; Pkg.instantiate()'
```

## Repository structure

### Data

The repository is structured as follow:

- `data/interim`: contains the data, downloaded from the [Zenodo record] and the
 NCBI and used as input for the pipeline.
- `data/outputs`: contains the annotated islands, as a collection of json files.
  These data can also be retrieved from the [figshare record].
- `data/post-process`: contains the post-processed data : the "base" contain the
  info on the islands (one row per island), and the "summary" contains
  aggregation accross the whole dataset.
- `./figures`: contains all the panel that constitute the figures of the paper.

### Notebooks

The notebooks are in the `scripts` folder. The subfolder `scibelt` contains a
local library with different routines used in the notebooks.

The first notebook (`build-db`) download the data, build the database and
provide some basic statistics on the data.

The second notebook (`pipeline-main`) run the pipeline on the data, and produce
the annotated islands. It's a very long process, even on a powerful machine. It
is recommended to run the atollgen-cli on a cluster thanks to the `atollgen-cli
batch` and `atollgen-cli sbatch` commands (see `lauch_jobs.sh` for an example of
what can generate the `sbatch --dry` subcommand).

For partial reproducibility, it is recommended to change the variable USE_IV4 to
False in the `build-db` notebook. This will skip the IV4 source that hold most
of the islands.

The third notebook (`post-process`) post-process the data, and produce the
tables and figures of the paper. This is a Julia file, that can be run with the
following command:

```bash
julia --project=. scripts/post-process.jl
```

The post-process step can be long depending on if you try to reproduce the whole
pipeline with the IV4 source or not.

The next notebooks (`aggregates`) is used to produce the "summary" data.

The remaining notebooks are used to produce the figures of the paper. The
`makefig` notebook should be the only one to be run, and will execute all the
`viz-...` notebooks that produce the figures (main and supp).

[Zenodo record]: https://zenodo.org/record/7866495
[figshare record]: TODO
[Atollgen project]: https://atollgen.gitlab.io/docs/modules/db.html