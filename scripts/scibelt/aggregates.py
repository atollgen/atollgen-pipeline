from __future__ import annotations

from collections import OrderedDict
from copy import deepcopy
from dataclasses import dataclass, field
from functools import cached_property
from pathlib import Path
from typing import Optional

import polars as pl
import yaml
from entrez_fetcher.genome_store import GenomeStore
from pkg_resources import resource_stream


def _process_index(name, df: pl.DataFrame):
    key_builder_query = (
        pl.col("accession")
        + ":"
        + pl.col("start").cast(str)
        + ".."
        + pl.col("end").cast(str)
    ).alias("index")
    df.insert_at_idx(0, df.select(key_builder_query).to_series())
    if name != "islands":
        df = df.drop("accession", "start", "end")
    return df


def _preprocess(name, df: pl.DataFrame):
    return _process_index(name, df)


def build_pivot_count(df, index, columns):
    count = df.groupby(index, columns).count()
    pivoted_count = count.pivot(
        index=index,
        columns=columns,
        values="count",
        aggregate_function=None,
    )
    return pivoted_count.fill_null(0)


def get_nth(df, col, n=10):
    return (
        df.select(pl.col("phylum").alias(col), pl.col(col).alias("count"))
        .sort(by="count", descending=True)
        .slice(0, n)
        .with_columns(
            pl.when(pl.col("count") > 0).then(pl.col(col)).otherwise(None).alias(col)
        )
        .drop("count")
    )


def ordered_load(stream, Loader=yaml.SafeLoader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))

    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, construct_mapping
    )
    return yaml.load(stream, OrderedLoader)


format_data = ordered_load(resource_stream("scibelt", "resources/format.yml"))


def aggregate_format(
    df: pl.DataFrame,
    *,
    index: Optional[str] = None,
    columns: Optional[str] = None,
    index_key: Optional[str] = None,
    columns_key: Optional[str] = None,
    reorder_index=True,
    reorder_columns=True,
    rename_index=True,
    rename_columns=True,
    total_index=False,
    total_columns=False,
    extra_cols=False,
):
    if index_key is None:
        index_key = index
    if columns_key is None:
        columns_key = columns
    if not extra_cols:
        extra_cols = []
    index_info = format_data.get(index_key, None)
    col_info = format_data.get(columns_key, None)

    df = deepcopy(df)
    if col_info:
        if reorder_columns:
            df = df.select(
                *([index] if index else []),
                *extra_cols,
                *[col for col in col_info if col in df.columns],
            )
        if rename_columns:
            df = df.rename(
                {
                    col: info.get("alias", col)
                    for col, info in col_info.items()
                    if info and col in df.columns
                }
            )
    if index_info:
        idx_series = pl.DataFrame(
            {
                index: index_info.keys(),
                "alias": [index_info[idx].get("alias", idx) for idx in index_info],
                "order": range(len(index_info)),
            }
        )
        if reorder_index:
            df = (
                df.join(idx_series, on=index, how="inner")
                .sort(by="order")
                .drop("order")
            )
        if rename_index:
            df = df.with_columns(pl.col("alias").alias(index)).drop("alias")
    if total_index:
        df = pl.concat(
            [df, df.drop(index).sum(axis=1).alias("total").to_frame()],
            how="horizontal",
        )
    if total_columns:
        df = pl.concat(
            [
                df,
                df.drop(index)
                .sum(axis=0)
                .with_columns(pl.lit("total").alias(index))
                .select(*df.columns),
            ],
            how="vertical",
        )
    return df


@dataclass
class Dataset:
    islands: pl.DataFrame
    cargo: pl.DataFrame
    categs: pl.DataFrame
    defensefinder: pl.DataFrame
    integ: pl.DataFrame
    integrase: pl.DataFrame
    origin: pl.DataFrame
    rgi_extra: pl.DataFrame
    rgi: pl.DataFrame
    trna: pl.DataFrame
    genome_store: GenomeStore = field(default_factory=GenomeStore, repr=False)

    @classmethod
    def from_folder(cls, folder, genome_store=None, preprocess=lambda dfs: dfs):
        folder = Path(folder)
        return cls(
            **preprocess(
                {
                    filename.stem: _preprocess(filename.stem, pl.read_csv(filename))
                    for filename in folder.glob("*.csv")
                }
            ),
            genome_store=genome_store,
        )

    @cached_property
    def accession(self) -> pl.Series:
        return self.islands.select("accession").unique().to_series(0)

    @cached_property
    def taxonomy(self) -> pl.DataFrame:
        tax_df = self.genome_store.get_taxonomy(self.accession.to_list())
        lineages = (
            tax_df.drop_duplicates("tax_id").set_index("tax_id").lineage_ex.dropna()
        )

        def process_lin(tax_id, lineage):
            return (
                pl.DataFrame(lineage)
                .with_columns(pl.lit(tax_id).alias("tax_id"))
                .select("tax_id", "rank", "scientific_name")
                .unique(subset="rank")
            )

        lineages = pl.concat(
            (process_lin(tax_id, lineage) for tax_id, lineage in lineages.items()),
            how="diagonal",
        )

        lineages = lineages.pivot(
            index="tax_id",
            columns="rank",
            values="scientific_name",
            aggregate_function=None,
        )

        return lineages

    @cached_property
    def phylum(self) -> pl.Series:
        lineage = self.taxonomy
        filled_lineage = (
            lineage.transpose()
            .fill_null(strategy="backward")
            .transpose(column_names=lineage.columns)
            .with_columns(pl.col("tax_id").cast(pl.Int64))
        )
        is_gammaprot_query = pl.col("class") == "Gammaproteobacteria"
        non_gammaprot_phylum = filled_lineage.filter(~is_gammaprot_query).select(
            "tax_id", "phylum"
        )
        gammaprots_phylum = filled_lineage.filter(is_gammaprot_query).select(
            "tax_id", (pl.col("order") + " Gammaproteo").alias("phylum")
        )
        phylum = pl.concat(
            [non_gammaprot_phylum, gammaprots_phylum], how="vertical"
        ).sort("tax_id")
        phylum = phylum.with_columns(
            pl.col("phylum").str.replace(
                "Gammaproteobacteria Gammaproteo", "Gammaproteobacteria Other"
            )
        )
        phylum = self.islands.join(phylum, on="tax_id", how="left").select(
            "index", "phylum"
        )
        phylum = phylum.with_columns(pl.col("phylum").fill_null("ND"))
        return phylum

    @cached_property
    def labels(self):
        return self.islands.select("index", "label")

    @cached_property
    def source_by_label(self):
        sources_query = pl.col("source").str.split(";").list.unique()
        is_mono_source = sources_query.list.lengths() == 1

        label_source_exclusive = self.islands.select(
            "label",
            pl.when(is_mono_source)
            .then(sources_query.list.first())
            .otherwise(pl.lit("combi")),
        )
        return build_pivot_count(label_source_exclusive, "label", "source")

    @cached_property
    def detection_by_label(self):
        df = self.islands.select(
            "index", "label", pl.col("detection").str.split(";")
        ).explode("detection")
        return build_pivot_count(df, "label", "detection")

    @cached_property
    def categs_by_label(self):
        return (
            self.categs.join(self.labels, on="index")
            .drop("index")
            .groupby("label")
            .mean()
        )

    @cached_property
    def phylum_by_label(self):
        return build_pivot_count(
            self.phylum.join(self.labels, on="index"), "phylum", "label"
        )

    @cached_property
    def phylum8_by_label(self):
        in_phyl8_query = pl.col("phylum").is_in(format_data["phylum_8"].keys())
        phyl8 = self.phylum_by_label.filter(in_phyl8_query)
        others = self.phylum_by_label.filter(~in_phyl8_query)
        return pl.concat(
            [phyl8, others.sum().with_columns(pl.lit("Others").alias("phylum"))]
        )

    @cached_property
    def most_common_phylum_by_label(self):
        most_common_phylum_by_label = pl.concat(
            [
                get_nth(self.phylum_by_label, col)
                for col in self.phylum_by_label.columns
                if col != "phylum"
            ],
            how="horizontal",
        )
        return most_common_phylum_by_label

    @cached_property
    def rgi_ncds_by_label(self):
        return build_pivot_count(
            self.rgi.join(self.labels, on="index"), "cds_count", "label"
        ).sort(by="cds_count")

    @cached_property
    def rgi_ar_count(self):
        rgi = self.rgi.drop("cds_count").melt(
            id_vars="index", variable_name="AR", value_name="count"
        )
        rgi = (
            rgi.with_columns(pl.col("AR").str.split("; "))
            .explode("AR")
            .groupby("index", "AR")
            .agg(pl.sum("count"))
            .pivot(index="index", columns="AR", values="count", aggregate_function=None)
            .fill_null(0)
        )
        return rgi

    @cached_property
    def rgi_ar_count_by_label(self):
        return (
            self.rgi_ar_count.join(self.labels, on="index")
            .groupby("label")
            .agg(pl.all().exclude("index").sum())
        )

    @cached_property
    def rgi_ar_count_by_phylum(self):
        rgi_phylum = (
            self.rgi_ar_count.join(self.phylum, on="index")
            .groupby("phylum")
            .agg(pl.all().exclude("index").sum())
        )
        columns = sorted(
            rgi_phylum.drop("phylum").columns,
            key=lambda x: rgi_phylum.select(x).sum().to_numpy().squeeze(),
            reverse=True,
        )
        rgi_phylum = rgi_phylum.select("phylum", *columns)
        return rgi_phylum

    @cached_property
    def rgi_nsys_by_label(self):
        nsys_rgi = self.rgi_ar_count.select(
            "index",
            pl.concat_list(pl.all().exclude("index") > 0).list.sum().alias("nsys"),
        )
        return build_pivot_count(
            nsys_rgi.join(self.labels, on="index"), "nsys", "label"
        ).sort(by="nsys")

    @cached_property
    def rgi_nsys_by_phylum_by_label(self):
        nsys_query = pl.concat_list(pl.all().exclude("index") > 0).list.sum()
        nsys_rgi = self.rgi_ar_count.select(
            "index",
            pl.when(nsys_query > 2)
            .then("2+")
            .otherwise(nsys_query.cast(str))
            .alias("nsys"),
        )
        return build_pivot_count(
            nsys_rgi.join(self.labels, on="index").join(self.phylum, on="index"),
            ["label", "nsys"],
            "phylum",
        ).sort(by=["label", "nsys"])

    @cached_property
    def cargo_by_label(self):
        return build_pivot_count(
            self.cargo.join(self.labels, on="index"), "label", "cargo_type"
        )

    @cached_property
    def cargo_by_phylum(self):
        return build_pivot_count(
            self.cargo.join(self.phylum, on="index"), "phylum", "cargo_type"
        )

    @cached_property
    def defensefinder_system_by_label(self):
        dfinder_nsys_df_bylabel = (
            self.defensefinder.join(self.labels, on="index")
            .groupby("label")
            .agg(
                [
                    (pl.col("nsystems_dfinder") == i).sum().alias(str(i))
                    for i in range(10)
                ],
            )
        )
        dfinder_nsys_df_bylabel = dfinder_nsys_df_bylabel.select(
            "label",
            *[
                col
                for col in dfinder_nsys_df_bylabel.columns
                if col != "label"
                and (dfinder_nsys_df_bylabel.select(col).to_series().sum() > 0)
            ],
        )
        return dfinder_nsys_df_bylabel

    @cached_property
    def defensefinder_ncds_by_label(self):
        return build_pivot_count(
            self.defensefinder.join(self.labels, on="index"), "cds_count", "label"
        ).sort(by="cds_count")

    @cached_property
    def defensefinder_by_label(self):
        return (
            self.defensefinder.drop("cds_count", "nsystems_dfinder")
            .join(self.labels, on="index")
            .with_columns(pl.col(pl.INTEGER_DTYPES).clip_max(1))
            .groupby("label")
            .agg(pl.all().exclude("index").sum())
        )

    @cached_property
    def defensefinder_by_phylum(self):
        return (
            self.defensefinder.drop("cds_count", "nsystems_dfinder")
            .join(self.phylum, on="index")
            .with_columns(pl.col(pl.INTEGER_DTYPES).clip_max(1))
            .groupby("phylum")
            .agg(pl.all().exclude("index").sum())
        )

    @cached_property
    def defensefinder_combination(self):
        dfinder_combi = self.defensefinder.drop("cds_count", "nsystems_dfinder")
        str_combi = dfinder_combi.select(
            "index",
            *(
                pl.when(pl.col(col) > 0).then(col).alias(col)
                for col in dfinder_combi.drop("index").columns
            ),
        )
        return str_combi.select(
            "index",
            pl.fold(
                acc=pl.lit(""),
                function=lambda acc, x: pl.when((acc == "") & x.is_not_null())
                .then(x)
                .when(x.is_not_null())
                .then(acc + ";" + x)
                .otherwise(acc),
                exprs=pl.all().exclude("index").alias("combi"),
            ).map_dict({"": None}, default=pl.first()),
        )

    @cached_property
    def defensefinder_combination_by_label(self):
        return build_pivot_count(
            self.defensefinder_combination.join(self.labels, on="index"),
            "combi",
            "label",
        )

    @cached_property
    def defensefinder_combination_by_phylum(self):
        return build_pivot_count(
            self.defensefinder_combination.join(self.phylum, on="index"),
            "combi",
            "phylum",
        )

    @cached_property
    def defensefinder_num_system_by_class_label(self):
        cut_nsys_query = (
            pl.when(pl.col("nsystems_dfinder") >= 2)
            .then(pl.lit("2+"))
            .otherwise(pl.col("nsystems_dfinder").cast(str))
            .alias("nsystems_dfinder")
        )

        nsys = (
            self.defensefinder.select("index", cut_nsys_query)
            .join(self.labels, on="index")
            .join(self.phylum, on="index")
        )
        return nsys.pivot(
            index=["label", "nsystems_dfinder"],
            columns="phylum",
            values="index",
            aggregate_function="count",
        ).fill_null(0)

    @cached_property
    def integ_site(self):
        is_cds = (pl.col("cds_bound_distance") < -50) & pl.col(
            "integ_site"
        ).str.contains("CDS")
        is_trna = pl.col("integ_site").str.contains("trna")
        return self.integ.select(
            "index",
            pl.when(is_cds)
            .then("CDS")
            .when(is_trna)
            .then("trna")
            .otherwise(pl.col("integ_site").fill_null("ND"))
            .alias("integ_site"),
        )

    @cached_property
    def integ_site_by_label(self):
        integ_site_by_label = build_pivot_count(
            self.integ_site.join(self.labels, on="index"), "label", "integ_site"
        )
        return integ_site_by_label

    @cached_property
    def integ_site_by_label_detection(self):
        df = (
            self.islands.select("index", pl.col("detection").str.split(";"), "label")
            .join(self.integ_site, on="index")
            .explode("detection")
        )

        return (
            df.groupby("detection", "label", "integ_site")
            .count()
            .pivot(
                values="count",
                columns="integ_site",
                index=["label", "detection"],
                aggregate_function=None,
            )
            .fill_null(0)
        )

    @cached_property
    def integrase_by_label(self):
        return (
            self.integrase.with_columns(pl.col("integ_pfam").str.split(";"))
            .join(self.islands.select("index", "label"), on="index")
            .explode("integ_pfam")
            .pivot(
                values="index",
                index="integ_pfam",
                columns="label",
                aggregate_function="count",
            )
            .fill_null(0)
            .drop_nulls()
        )

    @cached_property
    def distance_integ_int(self):
        distance_integ_int = self.integ_site.join(
            self.integ.select("index", "closest_int_categ"), on="index"
        )

        distance_integ_int = distance_integ_int.select(
            "index",
            "integ_site",
            pl.col("closest_int_categ")
            .str.split(";")
            .list.unique()
            .list.eval(
                pl.element().filter(~pl.element().is_in(["DTR", "INT_DTR_like"]))
            )
            .list.sort()
            .list.join(";"),
        ).join(self.labels, on="index")
        return distance_integ_int

    @cached_property
    def closest_int_categ_by_label(self):
        return build_pivot_count(
            self.distance_integ_int,
            "label",
            "closest_int_categ",
        )

    @cached_property
    def closest_int_categ_integ_site_by_label(self):
        return build_pivot_count(
            self.distance_integ_int.filter(
                ~pl.col("integ_site").is_in(["ND", "intergenic"])
            ),
            "label",
            "closest_int_categ",
        )

    @cached_property
    def closest_int_categ_by_integ_site(self):
        return build_pivot_count(
            self.distance_integ_int,
            "integ_site",
            "closest_int_categ",
        ).select(pl.all().exclude("null"))

    @cached_property
    def side_phase_by_label(self):
        return build_pivot_count(
            self.integ.join(self.labels, on="index"), "label", "side_phase"
        ).drop("null")

    @cached_property
    def strand_phase_by_label(self):
        return build_pivot_count(
            self.integ.join(self.labels, on="index"), "label", "strand_phase"
        ).drop("null")

    @cached_property
    def strand_phase_by_label_by_integ_site(self):
        return build_pivot_count(
            self.integ_site.join(self.labels, on="index").join(
                self.integ.select("index", "strand_phase"), on="index"
            ),
            ["strand_phase", "label"],
            "integ_site",
        ).filter(pl.col("strand_phase").is_not_null())

    @cached_property
    def opposite_by_label(self):
        return build_pivot_count(
            self.integ.join(self.labels, on="index"), "label", "opposite_env"
        ).drop("null")

    @cached_property
    def opposite_env_by_label_by_integ_site(self):
        return build_pivot_count(
            self.integ_site.join(self.labels, on="index").join(
                self.integ.select("index", "opposite_env"), on="index"
            ),
            ["opposite_env", "label"],
            "integ_site",
        ).filter(pl.col("opposite_env").is_not_null())

    @cached_property
    def integ_site_by_phylum(self):
        return build_pivot_count(
            self.integ_site.join(self.phylum, on="index"), "phylum", "integ_site"
        )

    @cached_property
    def most_common_integrase_combi_by_label(self):
        n = 10
        pfam_integs = self.integrase.join(self.labels, on="index")
        most_common_integrase_combi_by_label = (
            pfam_integs.groupby("label")
            .agg(pl.col("integ_pfam").value_counts(sort=True))
            .select(
                "label",
                *[pl.col("integ_pfam").list.get(i).alias(str(i)) for i in range(n)],
            )
            .melt(id_vars="label", value_name="integ_pfam", variable_name="rank")
            .sort(["label", "rank"])
        )
        label_count = self.labels.groupby("label").agg(
            pl.col("label").count().alias("total_count_in_label")
        )
        most_common_integrase_combi_by_label = (
            most_common_integrase_combi_by_label.join(label_count, on="label").select(
                "label",
                pl.col("rank").cast(int),
                pl.col("integ_pfam").struct.field("integ_pfam"),
                pl.col("integ_pfam").struct.field("counts"),
                (
                    pl.col("integ_pfam").struct.field("counts")
                    / pl.col("total_count_in_label")
                ).alias("freq"),
            )
        )
        most_common_integrase_combi_by_label = pl.concat(
            [
                most_common_integrase_combi_by_label.filter(pl.col("label") == label)
                .rename(
                    {
                        "integ_pfam": f"{label}_integ_pfam",
                        "counts": f"{label}_counts",
                        "freq": f"{label}_freq",
                    }
                )
                .drop("rank", "label")
                for label in most_common_integrase_combi_by_label["label"]
                .unique()
                .to_list()
                if label != "uGI"
            ],
            how="horizontal",
        ).insert_at_idx(0, pl.Series("rank", range(n)))
        return most_common_integrase_combi_by_label
