import re
from dataclasses import dataclass
from pathlib import Path
from typing import Sequence

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import pandas as pd
import seaborn as sns
from IPython.display import display
from PIL import Image
from scipy.stats import ttest_ind

from .aggregates import format_data


def fix_labels(mylabels, tooclose=0.1, sepfactor=2):
    vecs = np.zeros((len(mylabels), len(mylabels), 2))
    dists = np.zeros((len(mylabels), len(mylabels)))
    for i in range(0, len(mylabels) - 1):
        for j in range(i + 1, len(mylabels)):
            a = np.array(mylabels[i].get_position())
            b = np.array(mylabels[j].get_position())
            dists[i, j] = np.linalg.norm(a - b)
            vecs[i, j, :] = a - b
            if dists[i, j] < tooclose:
                mylabels[i].set_x(a[0] + sepfactor * vecs[i, j, 0])
                mylabels[i].set_y(a[1] + sepfactor * vecs[i, j, 1])
                mylabels[j].set_x(b[0] - sepfactor * vecs[i, j, 0])
                mylabels[j].set_y(b[1] - sepfactor * vecs[i, j, 1])


def latex_size(
    double=False, ratio=1.618, width=None, height=None, width_prop=1.0, fontsize=6.5
):
    if width is None:
        width = 7.0 if double else 3.3
    width *= width_prop
    if height is None:
        height = width / ratio
    return plt.rc_context(
        rc={
            "figure.figsize": (width, height),
            "font.size": fontsize,
        }
    )


def check_bw(color):
    r, g, b, *_ = color
    return "#000000" if (r * 0.299 + g * 0.587 + b * 0.114) > 150 / 255 else "#ffffff"


def add_bar_label(ax, ref_df, thresh=0.05, inside=True, total=True, alternate=False):
    if inside:
        for i, container in enumerate(ax.containers):
            labels = ref_df.iloc[:, i].to_list()
            labels = [
                f"{label:,}" if prop > thresh else ""
                for label, prop in zip(labels, container.datavalues)
            ]
            label_color = [
                check_bw(patch.get_facecolor()) for patch in container.patches
            ]
            txts = ax.bar_label(
                container,
                labels,
                label_type="center",
                fontsize=6,
            )
            for tcolor, txts in zip(label_color, txts):
                txts.set_color(tcolor)

    if total:
        for i, value in enumerate(ref_df.sum(axis=1)):
            ax.text(
                i,
                1.02 if not alternate or i % 2 else 1.1,
                f"{int(value):,}",
                ha="center",
                fontstyle="italic",
                fontsize=6,
            )

    return ax


def propbarplot(
    df,
    kind="bar",
    ax=None,
    barlabel=True,
    count_label=True,
    total_label=True,
    alternate=False,
    barlabel_thresh=0.1,
    width=0.9,
    *args,
    **kwargs,
):
    if ax is None:
        _, ax = plt.subplots()
    norm = (df.T / df.T.sum()).T
    norm.plot(kind=kind, stacked=True, ax=ax, width=width, *args, **kwargs)
    if barlabel:
        add_bar_label(
            ax,
            df,
            thresh=barlabel_thresh,
            inside=count_label,
            total=total_label,
            alternate=alternate,
        )
    if kind == "bar":
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    else:
        ax.xaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    ax.set_axisbelow(True)
    return ax


def get_legend_hl(ax, reverse=True):
    step = -1 if reverse else 1
    height, width = ax.get_legend_handles_labels()
    return height[::step], width[::step]


@dataclass
class FigureSaver:
    fig_path: Path
    fmt: Sequence[str] = ("svg", "png")
    dpi: int = 600
    display_res: bool = True
    transparent: bool = True

    def __post_init__(self):
        self.fig_path = Path(self.fig_path)
        self.fig_path.mkdir(parents=True, exist_ok=True)

    def savefig(
        self,
        fig,
        name,
        fmt=None,
        dpi=None,
        display_res=None,
        transparent=None,
        *args,
        **kwargs,
    ):
        if fmt is None:
            fmt = self.fmt
        if dpi is None:
            dpi = self.dpi
        if display_res is None:
            display_res = self.display_res
        if transparent is None:
            transparent = self.transparent

        if isinstance(fmt, str):
            fmt = [fmt]
        for format in fmt:
            fig.savefig(
                self.fig_path / f"{name}.{format}",
                bbox_inches="tight",
                dpi=dpi,
                transparent=transparent,
                *args,
                **kwargs,
            )
        if "png" in fmt and display_res:
            img = Image.open(self.fig_path / f"{name}.png")
            w, h = img.size
            img = img.resize((600, round(h / w * 600)))
            background = Image.new("RGBA", img.size, (255, 255, 255))
            img = Image.alpha_composite(background, img)
            dst = Image.new("RGB", (img.width, 2 * img.height))
            dst.paste(img, (0, 0))
            dst.paste(img.convert("L"), (0, img.height))
            display(dst)
            plt.close()


def replace_ar(x):
    match = re.match(
        r"^(?P<pre>antibiotic)?\s*(?P<mec>.*?)\s*(?P<suf>to antibiotic)?$", x
    )
    gd = match.groupdict()
    if not any([gd["pre"], gd["suf"]]):
        return x
    mec = match.groupdict()["mec"]
    return " ".join(
        [word.capitalize() if i == 0 else word for i, word in enumerate(mec.split(" "))]
    )


def process_df(df: pd.DataFrame, psi=False):
    df = df.copy()
    if not psi:
        df = df.drop("psiICE", errors="ignore").drop("psiICE", axis=1, errors="ignore")
    for replacement in [
        {"psiICE": "$\mathrm{\psi ICE}$", "INT_ser": "INT Ser", "INT_tyr": "INT Tyr"},
        {"rgi": "AR", "defensefinder": "DS", "rgi & defensefinder": "AR & DS"},
        {"CDS": "Disr. CDS", "ambiguous": "Ambiguous", "intergenic": "Intergenic"},
        lambda x: x.replace("trna", "tRNA"),
        replace_ar,
        lambda x: x.replace("_", " "),
    ]:
        try:
            df = df.rename(index=replacement)
        except Exception:
            pass

        try:
            df = df.rename(columns=replacement)
        except Exception:
            pass

    return df


def sep_count_on(df, keys, axis=0, keep_other="Others"):
    keys = [key for key in keys if key != keep_other]
    if axis == 0:
        keept = df.loc[keys]
        if keep_other is not None:
            keept.loc[keep_other] = df.loc[~df.index.isin(keept.index)].sum(axis == 1)
        return keept
    if axis == 1:
        keept = df.loc[:, keys]
        if keep_other is not None:
            keept.loc[:, keep_other] = df.loc[:, ~df.columns.isin(keept.columns)].sum(
                axis == 1
            )
        return keept
    raise ValueError("axis should be 0 or 1")


def label_size(df, group, palette=None):
    df = df.copy()
    mapper = {
        idx: f"{idx}\n({size:,})" for idx, size in df.groupby(group).size().iteritems()
    }
    df[group] = df[group].map(mapper)
    if palette is None:
        return df
    palette = {mapper.get(key, f"{key} (0)"): value for key, value in palette.items()}
    return df, palette


def alias_or_name(format_dict):
    return [info.get("alias", name) for name, info in format_dict.items()]


kb_formatter = mpl.ticker.FuncFormatter(lambda x, pos: f"{x / 1000:.0f}")


def seqlen_plot(
    df,
    on,
    on_key=None,
    ax=None,
    remove_empty=True,
    reverse_order=False,
    *args,
    **kwargs,
):
    if on_key is None:
        on_key = on
    if ax is None:
        fig, ax = plt.subplots()
    palette = {
        real_name: info["color"]
        for name, info in format_data[on_key].items()
        if remove_empty and (real_name := info.get("alias", name)) in df[on].unique()
    }
    df, palette = label_size(df, on, palette=palette)
    with plt.rc_context({"lines.markersize": 2}):
        sns.boxenplot(
            x=on,
            y="seq_len",
            data=df,
            ax=ax,
            linewidth=0.6,
            order=list(palette.keys())[:: -1 if reverse_order else 1],
            palette=list(palette.values())[:: -1 if reverse_order else 1],
            *args,
            **kwargs,
        )
    ax.yaxis.set_major_formatter(kb_formatter)
    ax.set_ylabel("Sequence length (kb)")
    ax.grid(axis="y")
    ax.set_axisbelow(True)
    return ax
