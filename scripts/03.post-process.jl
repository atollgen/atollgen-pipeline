# %%
using DataFrames
using LazyJSON, CSV, YAML
using Glob
using Logging
using DataStructures, FreqTables, NamedArrays, Missings, Intervals, LinearAlgebra, Statistics, DataFramesMeta
import Base:length

length(i::Interval) = abs(i.last - i.first)

all_categs = open("./data/interim/categs.json") do f
    txt = read(f, String)
    catmap = LazyJSON.value(txt; getproperty=true)
    Iterators.flatten(values(catmap)) |> unique .|> Symbol
end;

function is_empty_island(filename)
    open(filename) do f
        jsontxt = read(f, String)
        return isempty(jsontxt)
    end
end

function parse_island_res(filename)
    open(filename) do f
        txt = read(f, String)
        txt = replace(txt, "NaN" => "null")
        txt = replace(txt, "MPSA_like" => "INT_DTR_like")
        LazyJSON.value(txt; getproperty=true)
    end
end


# %%
islands_df = CSV.read("./data/interim/islands.csv", DataFrame);
genomes_df = unique(CSV.read("./data/interim/genomes_info.csv", DataFrame), [:accession]);
islands_df = leftjoin(islands_df, genomes_df[!, ["accession", "desc", "kind"]], on=:accession);

# %%
res_files = @. "./data/outputs/" * islands_df.accession * "_" * string(islands_df.start) * "-" * string(islands_df.end) * ".json"
islands = parse_island_res.(filter(!is_empty_island, res_files)) |> collect;
labels_df = DataFrame(getproperty.(islands, ["accession" "start" "end" "label"]), ["accession", "start", "end", "label"]);
labels_df = @rtransform labels_df :accession = String(:accession) :start = Int(:start) :end = Int(:end);
islands_df = leftjoin(islands_df, labels_df, on=[:accession, :start, :end]);

# %%
idxs = DataFrame(getproperty.(islands, ["accession" "start" "end"]), [:accession, :start, :end])
idxs[!, :start] = Int.(idxs[!, :start])
idxs[!, :end] = Int.(idxs[!, :end])
islands_df = rightjoin(islands_df, idxs, on=[:accession, :start, :end]);

# %%
function get_noverlaps(starts, ends)
    all_intervals = @. Interval(Int(starts), Int(ends))
    res = OrderedDict{Symbol,Any}[]
    for interval in all_intervals
        rel_intervals = all_intervals[all_intervals .!= interval]
        intersects = @. intersect(interval, rel_intervals)
        noverlaps = sum(@. !isempty((intersects)))
        l_overlaps = filter(!iszero, length.(intersects))
        mean_inter = !isempty(l_overlaps) ? mean(l_overlaps) : missing
        std_inter = !isempty(l_overlaps) ? std(l_overlaps) : missing
        min_inter, q25, q50, q75, max_inter = !isempty(l_overlaps) ? quantile(l_overlaps, [0, 0.25, 0.5, 0.75, 1.0]) : [missing, missing, missing, missing, missing]
        push!(res, OrderedDict(
            :noverlaps => noverlaps,
            :mean_overlaps => mean_inter,
            :std_overlaps => std_inter,
            :min_overlaps => min_inter,
            :q25_overlaps => q25,
            :q50_overlaps => q50,
            :q75_overlaps => q75,
            :max_overlaps => max_inter,
            )
        )
    end
    return res
end

gdf = groupby(islands_df, :accession)
islands_df_overlaps = transform(gdf, [:start, :end] => get_noverlaps => AsTable);

# %%
function categ_cut_cds(island; threshold=0, reentrant_threshold=50)
    if island.label ∈ ["GI", "uGI"]
        return (:integ_site => missing, :cds_size => missing, :cds_bound_distance => missing, :side_phase => missing, :strand_phase => missing, :opposite_env => missing, :closest_int_categ => missing)
    end
    ints = [
        cds
        for cds in values(island.CDSs)
        if !isempty([annot for annot in cds.annotations if annot.origin == "integrase"])
    ]
    closest_int = first(sort(ints, by=(cds) -> Int(cds.bound_distance)))
    int_side = replace(closest_int.area, "_ext" => "")
    int_strand = closest_int.strand
    categs_int = []
    for annot in closest_int.annotations
        if annot.origin  == "integrase"
            push!(categs_int, Symbol.(split(annot.quals.categs, ";"))...)
        end
    end
    categ_int = join(categs_int, ";")


    cut_cdss = [
        cds for cds in values(island.CDSs)
        if (occursin("env", cds.area) && cds.bound_distance <= threshold)
    ]

    size_left_env = abs(island.coords.start_island - island.coords.left_env)
    size_right_env = abs(island.coords.end_island - island.coords.right_env)

    n_cut_cds = length(cut_cdss)

    if (size_left_env <= threshold) && (size_right_env <= threshold)
        return (:integ_site => "ND", :cds_size => missing, :cds_bound_distance => missing, :side_phase => missing, :strand_phase => missing, :opposite_env => missing, :closest_int_categ => categ_int)
    end
    if n_cut_cds == 0
        if reentrant_threshold > 0
            return categ_cut_cds(island; threshold=reentrant_threshold, reentrant_threshold=0)
        end
        return (:integ_site => "intergenic", :cds_size => missing, :cds_bound_distance => missing, :side_phase => missing, :strand_phase => missing, :opposite_env => missing, :closest_int_categ => categ_int)
    end
    if n_cut_cds > 1
        return (:integ_site => "ambiguous", :cds_size => missing, :cds_bound_distance => missing, :side_phase => missing, :strand_phase => missing, :opposite_env => missing, :closest_int_categ => categ_int)
    end

    cds = first(cut_cdss)
    cds_size = abs(cds["start"] - cds["end"])
    cds_bound_distance = Int(cds.bound_distance)
    kind = any([annot.origin == "trnascan" for annot in cds.annotations]) ? "trna" : "CDS"
    strand = cds.strand
    side = replace(cds.area, "_env" => "")

    orient = (
        (side == "left" && strand == "+") || (side == "right" && strand == "-") ? "3'" : "5'"
        )

    if kind == "trna"
        if abs(cds_bound_distance) == cds_size / 2
            orient = "3'"
        elseif abs(cds_bound_distance) > cds_size / 2
            orient = orient == "3'" ? "5'" : "3'"
        end
    end

    opposite_side = side == "left" ? "right" : "left"
    opposite_cdss = [cds for cds in values(island.CDSs) if cds.area == "$(opposite_side)_env"]
    if !isempty(opposite_cdss)
        closest_opposite = first(sort(opposite_cdss, by=(cds) -> Int(cds.bound_distance)))
        is_closest_trna = any([annot.origin == "trnascan" for annot in closest_opposite.annotations])
        opposite = is_closest_trna ? "trna" : "CDS"
    else
        opposite = "no opposite"
    end


    return (
        :integ_site => "$(kind)_$(orient)",
        :cds_size => cds_size,
        :cds_bound_distance => Int(cds.bound_distance),
        :side_phase => int_side == side ? "same side int" : "opposite side int",
        :strand_phase => int_strand == strand ? "same strand int" : "opposite strand int",
        :opposite_env => opposite,
        :closest_int_categ => categ_int
    )
end

integ_df = DataFrame(Dict{Symbol,Any}.(categ_cut_cds.(islands)))
select!(integ_df, [:integ_site, :cds_size, :cds_bound_distance, :closest_int_categ, :side_phase, :strand_phase, :opposite_env])
freqtable(integ_df, :integ_site);

# %%
function extract_categs(island)
    categs = Symbol[]
    for CDS in values(island.CDSs)
        for annot in CDS.annotations
            if annot.origin ∈ ("mobility", "integrase")
                push!(categs, Symbol.(split(annot.quals.categs, ";"))...)
            end
        end
    end
    categ_count = counter(categs)
    for categ in all_categs
        if !(categ in keys(categ_count))
            categ_count[categ] = 0
    end
    end
    return categ_count
end

categs_df = DataFrame(Dict{Symbol,Any}.(extract_categs.(islands)));

# %%
all_origins = Symbol.(["integrase", "mobility", "pfam-A", "rgi", "defense_finder"])
function extract_origins(island)
    origins = Symbol[]
    for CDS in values(island.CDSs)
        for annot in CDS.annotations
            push!(origins, Symbol(annot.origin))
        end
    end
    origin_count = counter(origins)
    for origin in all_origins
        if !(origin in keys(origin_count))
            origin_count[origin] = 0
        end
    end
    return origin_count
end

origins_df = DataFrame(Dict{Symbol,Any}.(extract_origins.(islands)));


# %%
function cargo_info(row)
    if row.rgi > 0 && row.defense_finder == 0
        return "rgi"
    elseif row.rgi == 0 && row.defense_finder > 0
        return "defensefinder"
    elseif row.defense_finder > 0 && row.defense_finder > 0
        return "rgi & defensefinder"
    else
        return "none"
    end
end

cargo_df = DataFrame(:cargo_type => cargo_info.(eachrow(origins_df)));

# %%
function extract_dfinder_info(island)
    info = Symbol[]
    for CDS in values(island.CDSs)
        for annot in CDS.annotations
            if annot.origin  == "defense_finder"
                type = Symbol(annot.quals.type)
                if type == :RM
                    type = Symbol(annot.quals.subtype)
                end
                push!(info, type)
            end
        end
    end
    if !isempty(info)
        return counter(info)
    else
        return Dict()
    end
end

dfinder_info = extract_dfinder_info.(islands)
all_dfinder_types = reduce(union, keys.(dfinder_info))
dfinder_info = DataFrame(
        map(dfinder_info) do d
        new_d = Dict{Symbol,Int}()
        for key in all_dfinder_types
            new_d[key] = key ∈ keys(d) ? d[key] : 0
        end
        return new_d
    end
)
nsystems_dfinder_df = DataFrame(:nsystems_dfinder => sum.(eachrow(dfinder_info .> 0)));

# %%
function extract_integrase_ids(island)
    if island.label == "uGI"
        return missing
    end
    ints = [
        cds
        for cds in values(island.CDSs)
        if !isempty([annot for annot in cds.annotations if annot.origin == "integrase"])
    ]
    closest_int = first(sort(ints, by=(cds) -> Int(cds.bound_distance)))
    pfam_ids = []
    for annot in closest_int.annotations
        if annot.origin  == "integrase"
            push!(pfam_ids, annot.quals.query_id)
        end
    end
    pfam_ids = join(sort(pfam_ids), ";")
    return pfam_ids
end

integrase_ids = DataFrame(integ_pfam=extract_integrase_ids.(islands));

# %%
function extract_rgi_info(island)
    info = Symbol[]
    for CDS in values(island.CDSs)
        for annot in CDS.annotations
            if annot.origin  == "rgi"
                push!(info, Symbol(annot.quals.resistance_mechanism))
            end
        end
    end
    res = counter(info)
    return res
end

rgi_info = extract_rgi_info.(islands)
all_rgi_types = reduce(union, keys.(rgi_info))
rgi_info = DataFrame(
    map(rgi_info) do d
        new_d = Dict{Symbol,Int}()
        for key in all_rgi_types
            new_d[key] = key ∈ keys(d) ? d[key] : 0
        end
        return new_d
    end
);

# %%
function extract_rgi_extra(island)
    info = NTuple{2, Symbol}[]
    for CDS in values(island.CDSs)
        for annot in CDS.annotations
            if annot.origin  == "rgi"
                push!(info, (Symbol(annot.quals.resistance_mechanism), Symbol(annot.quals.drug_class)))
            end
        end
    end
    res = counter(info)
    return res
end

rgi_extra = extract_rgi_extra.(islands)
all_rgi_extra = reduce(union, keys.(rgi_extra))
rgi_extra = DataFrame(
    map(rgi_extra) do d
        new_d = Dict{Symbol,Int}()
        for key in all_rgi_extra
            new_d[Symbol(join(String.(key), ":"))] = key ∈ keys(d) ? d[key] : 0
        end
        return new_d
    end
);


# %%
function extract_rgi_cds(island)
    cds_count = 0
    for CDS in values(island.CDSs)
        if !isempty(CDS.annotations) && any([annot.origin  == "rgi" for annot in CDS.annotations])
            cds_count += 1
        end
    end
    return cds_count
end

rgi_cds_count = DataFrame(cds_count=extract_rgi_cds.(islands));

# %%
function extract_dfinder_cds(island)
    cds_count = 0
    for CDS in values(island.CDSs)
        if !isempty(CDS.annotations) && any([annot.origin  == "defense_finder" for annot in CDS.annotations])
            cds_count += 1
        end
    end
    return cds_count
end

dfinder_cds_count = DataFrame(cds_count=extract_dfinder_cds.(islands));

# %%
trna_count = DataFrame(trna_env_count=map(islands) do island
    trna_counter = 0
    for CDS in values(island.CDSs)
        if occursin("env", CDS.area) && !isempty(CDS.annotations) && any([annot.origin == "trnascan" for annot in CDS.annotations])
            trna_counter += 1
        end
    end
    return trna_counter
end);

# %%
dfs = (
    "islands" => islands_df_overlaps[!, [:accession, :start, :end, :source, :noverlaps, :seq_len, :label, :tax_id, :detection, :kind]],
    "categs" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], categs_df),
    "origin" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], origins_df),
    "integ" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], integ_df),
    "integrase" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], integrase_ids),
    "rgi" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], rgi_cds_count, rgi_info),
    "rgi_extra" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], rgi_extra),
    "trna" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], trna_count),
    "cargo" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], cargo_df),
    "defensefinder" => hcat(islands_df_overlaps[!, [:accession, :start, :end, :noverlaps]], dfinder_cds_count, nsystems_dfinder_df, dfinder_info)
    );

# %%
mkpath("./data/post-process/base_nooverlaps")
for (name, df) in dfs
    @show name
    df = df[df.noverlaps .== 0, :]
    df = df[:, Not(:noverlaps)]
    CSV.write("./data/post-process/base_nooverlaps/$name.csv", df)
end

mkpath("./data/post-process/base_overlaps")
for (name, df) in dfs
    @show name
    df = df[:, Not(:noverlaps)]
    CSV.write("./data/post-process/base_overlaps/$name.csv", df)
end

CSV.write("./data/post-process/all_islands_with_overlaps_data.csv", islands_df_overlaps)

# %%
